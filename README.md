Illwieckz' camera samples
=========================

This is a collection of camera produced files.

It currently contains some samples from:

- Canon EOS 5D Mark II with Magic Lantern and Astrodon variations
- Nikon D5100 and D7000 with Nikon Hackers variations
- Zoom H2n audio recorder, not a camera

Author
------

Thomas Debesse <dev@illwieckz.net>

Legal
-----

These files are distributed under the highly permissive [Creative Commons Zero 1.0](LICENSE.md) “Public Domain Dedication” (CC0 1.0).

An original of this legal code can be found [here](https://creativecommons.org/publicdomain/zero/1.0/legalcode) and a summary can be found [here](https://creativecommons.org/publicdomain/zero/1.0/).
